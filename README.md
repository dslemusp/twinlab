## Overview

This custom library block populates slave information from a TwinCAT solution to automatically create linked input/ouput variables from Simulink and TwinCAT. It uses the TE1400 library blocks `TC Module Input` and `TC MOdule Output`. With this there is no need to manually link generic `in`, `out` simulink blocks to input/outputs in TwinCAT.

## Requirements

- TwinCAT 3
- Matlab Simulink (Tested with matlab 2018b)
- Installation of [Beckhoff TE1400](https://infosys.beckhoff.com/english.php?content=../content/1033/te1400_tc3_target_matlab/index.html&id=)

## Installation
In matlab open the root folder of this repository and run the `setup.m` file.

## Usage

![](docs/usage.gif)

## ToDo

- Populate more than 1 Master. So far only the first Ethercat master containing a AMSNetID will be used.
- Populate input/outputs from master (Only IOs from slaves are being populated)
- Populate TwinCAT WcState and InfoData fields from slaves. So far only hardware related inputs/outputs from slaves are being populated.
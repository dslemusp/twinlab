%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Configuration file to add custom block to Beckhoff library and enable
% extra features for terminators, vector concatenate and bus creation.

% Daniel Lemus 2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get current directory
curr_dir = pwd;
% Add source folder to path
addpath([curr_dir '\src']);
savepath;

%% Copy block to Beckhoff Twincat Simulink Library
fprintf('Adding custom library block. Please wait... \n');
% Check if Beckhoff Library exist
if getSimulinkBlockHandle('TcTargetLib/TC Module Input',true) == -1
    error('Beckhoff TE1400 is not installed. Please install library first!')
end
TCLib = load_system('TcTargetLib');
% Unlock library
set_param(TCLib,'Lock','off');

TCBlks = get_param(TCLib,'Blocks');
for i = 1 : numel(TCBlks)
    TCBlk_i = TCBlks{i};
    blk_h = getSimulinkBlockHandle([get_param(TCLib,'Name') '/' TCBlk_i]);
    set_param(blk_h,'MaskSelfModifiable','on');
end


% Copy custom library to create slaves from existing TC proj
mSys = get(load_system('TcSlaveCreator'));
Blk_src = 'TC Slave Creator';

% Delete blk if any
if getSimulinkBlockHandle(['TcTargetLib/' Blk_src],true) ~= -1
    delete_block(['TcTargetLib/' Blk_src]);
end
h = add_block([mSys.Path '/' Blk_src],['TcTargetLib/' Blk_src]);
% Configure block callbacks
set_param(h,'CopyFcn',['initTCSubsystemVars(gcb); ',...
                       'set_param(gcb,''OpenFcn'',''TC_mask_config(gcb)'');',...
                       'set_param(gcb,''LinkStatus'',''none'');',...
                       'set_param(gcb,''CopyFcn'','''');']);
% set_param(h,'OpenFcn','TC_mask_config(gcb);');
set_param(h,'MaskSelfModifiable','on');
set_param(h,'Position',[250   129   385   181]);

% Lock Library
set_param(TCLib,'Lock','on');
save_system(TCLib);
close_system(TCLib);
fprintf('Library block copied succesfully!\n');

% Add extra contex menu options
sl_refresh_customizations;
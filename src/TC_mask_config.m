function TC_mask_config(block)
    % Populates info from TCInfo pertaining slaves and masters and puts it
    % in the block mask for further selection
    
    % Daniel Lemus
    % 2021
    UserData = get_param(block,'UserData');
    if isempty(UserData)
        TCInfo = '';
    else 
        TCInfo = UserData.TCInfo;
    end
    mymask = Simulink.Mask.get(block);
    
    % Get Parameter index for master
    master_idx      = strcmp('master',{mymask.Parameters.Name});
    master_param    = mymask.Parameters(master_idx);
    
    % Get Parameter index for popup slaves
    slave_idx = strcmp('slave',{mymask.Parameters.Name});
    slave_popup = mymask.Parameters(slave_idx);
    
    if ~isempty(TCInfo)
        % Add names to mask parameters
        master_param.Value = TCInfo.master.name;
        slaves_names = cellfun(@(x) x.name,TCInfo.slaves,'UniformOutput',false);
        slave_popup.TypeOptions = slaves_names;        
    else
        master_param.Value = '';
        slave_popup.TypeOptions = {''};
    end
    open_system(block,'mask');
end

function initTCSubsystemVars(myBlock)
    UserData.TCInfo = loadTCinfo();
    UserData.SlaveIdx = -1;
    set_param(myBlock,'UserData',UserData);
    clear('UserData')
end
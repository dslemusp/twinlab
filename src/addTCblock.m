function addTCblock(TCblock_id,TCInfo,TCSlave_idx,Ts)
% Creates a subsystem with input outputs in a specified simulink model

% Check if Ts is defined
if isempty(Ts)
    error('Sample time expression is empty or undefined');
end

% Check if Ts is number or str
if isnumeric(Ts)
   Ts = num2str(Ts); 
end

try 
%     TCblock_h = add_block('built-in/Subsystem',TCblock_id);
    % Clean subsystem
    Simulink.SubSystem.deleteContents(TCblock_id);
    if isempty(TCInfo)
        return;
    end
    
    % Appends name to user data (to prevent auto-refresh of the block in
    % the same slave selection. It is problematic during build)
%     UserData.SlaveIdx = TCSlave_idx;
%     set_param(TCblock_id,'UserData',UserData.SlaveName);
    
    TCblock_h = get_param(TCblock_id,'handle');

    blocks  = {TCInfo.slaves{TCSlave_idx}.inputs,...
               TCInfo.slaves{TCSlave_idx}.outputs};
    
    % Loop over inputs and outputs
    for i = 1 : length(blocks)
        in_outs = blocks{i};
        in_out = {'In','Out'};        
        x_pos = i-1;
        y_min = 0;
        % Loop over pdos
        for pdo_i = 1 : length(in_outs)
            pdo_name = remove_unsopported_characters(in_outs{pdo_i}.pdo_name);
            channels = in_outs{pdo_i}.channels;                    
            channel_names = {};
            
            % Clear port handles array
            tc_block_in_ph  = [];
            tc_block_out_ph = [];
            cast_blk_out_ph = [];  
            
            % Loop over channels
            for j = 1 : length(channels)
                channel = channels{j};
                channel_names{j} = remove_unsopported_characters(channel.Name);
                % Block Position
                left        = 10 + (i-1)*1000 ;
                right       = left + 30; 

                
    
                % Add TC Input/Outputs blocks
                tc_block_id     = sprintf('%s/Tc_%s',TCblock_id,channel_names{j});
                if strcmpi(in_out{i},'in')
                    % Add input                                                                                      
                    block_pos(1)    = left + 250;
                    block_pos(3)    = right + 325;
                    block_pos(2)    = y_min + (j-1)*35;
                    block_pos(4)    = block_pos(2) + 15; 

                    tc_block_h      = add_block('TcTargetLib/TC Module Output',...
                                           tc_block_id,...
                                           'Position',          block_pos,...
                                           'PortDim',           '1',...
                                           'SampleTime',        Ts,...
                                           'PortDataType',      channel.DataType,...
                                           'LinkToNode',        channel.FullName...
                                           );
                    temp_ph         = get_param(tc_block_h,'PortHandles'); 
                    tc_block_in_ph(j)  = temp_ph.Inport; 
                else
                    % Add output
                    block_pos(1)    = left - 425;
                    block_pos(3)    = right - 350;
                    block_pos(2)    = y_min + (j-1)*35;
                    block_pos(4)    = block_pos(2) + 15; 

                    tc_block_h      = add_block('TcTargetLib/TC Module Input',...
                                           tc_block_id,...
                                           'Position',          block_pos,...
                                           'PortDim',           '1',...
                                           'SampleTime',        Ts,...
                                           'PortDataType',      channel.DataType,...
                                           'LinkToNode',        channel.FullName...
                                           );
                    temp_ph         = get_param(tc_block_h,'PortHandles'); 
                    set(temp_ph.Outport, 'SignalNameFromLabel', channel_names{j})
                    tc_block_out_ph(j)  = temp_ph.Outport; 
                    
                    % Add cast to double
%                     cast_pos = block_pos + [1 0 1 0];
%                     cast_pos([1,3]) = []
                    cast_id  = sprintf('%s/cast_%s',TCblock_id,channel_names{j});
                    cast_blk = add_block(...
                                        'simulink/Quick Insert/Signal Attributes/Cast To Double',cast_id,...
                                        'Position',       [left-205 block_pos(2) left-170 block_pos(4)]...
                                        );%,...
                    cast_blk_ph         = get_param(cast_blk,'PortHandles'); 
                    set(cast_blk_ph.Outport, 'SignalNameFromLabel', channel_names{j})
                    cast_blk_out_ph(j) = cast_blk_ph.Outport;
                    
                    % Connect blocks
                    add_line(TCblock_id,tc_block_out_ph(j),cast_blk_ph.Inport,'autorouting','smart');
                end
            end

            y_max           = block_pos(4);
            top             = y_min + (y_max - y_min)/2 - 7;
            bottom          = top + 14;
            inout_blk_pos   = [left top right bottom];

            % Block Name
            block_id = sprintf('%s/%s',TCblock_id,pdo_name);

            % Add simulink input/output blocks
            block_h = add_block(...
                          sprintf('simulink/Commonly Used Blocks/%s1',in_out{i}),block_id,...
                          'Position',       inout_blk_pos);                          

            % Add bus blocks between TC in/out and Simulink in/outs
            if strcmpi(in_out{i},'in')
                % Add bus creator
                bus_id = sprintf('%s/Bus_%s',TCblock_id,pdo_name);
                if numel(channel_names) > 1
                    temp = strcat(channel_names(1:end-1),',');
                    output_signals = [strcat(temp{:}) channel_names{end}];
                else
                    output_signals = char(channel_names);
                end
                bus_h = add_block(...
                          'simulink/Commonly Used Blocks/Bus Selector',bus_id,...
                          'Position',       [left+100 y_min-5 left+105 y_max+5],...
                          'OutputSignals', output_signals...
                          );%,...
                bus_ph      = get_param(bus_h,'PortHandles');
                block_ph    = get_param(block_h,'PortHandles');
                add_line(TCblock_id,block_ph.Outport,bus_ph.Inport,'autorouting','smart');
                set(block_ph.Outport, 'SignalNameFromLabel', pdo_name)


                add_line(TCblock_id,bus_ph.Outport,tc_block_in_ph,'autorouting','smart');

            else
                
                % Add bus selector
                bus_id = sprintf('%s/Bus_%s',TCblock_id,pdo_name);
                n_inputs = length(channel_names);
                bus_h = add_block(...
                          'simulink/Commonly Used Blocks/Bus Creator',bus_id,...
                          'Position',       [left-105 y_min-5 left-100 y_max+5],...
                          'Inputs',         num2str(n_inputs)...
                          );%,...
                bus_ph      = get_param(bus_h,'PortHandles');
                block_ph    = get_param(block_h,'PortHandles');
                add_line(TCblock_id,bus_ph.Outport,block_ph.Inport,'autorouting','smart');
                set(bus_ph.Outport, 'SignalNameFromLabel', pdo_name)

                
                add_line(TCblock_id,cast_blk_out_ph,bus_ph.Inport,'autorouting','smart');
            end

            y_min = block_pos(4) + 40;
        end
                
    end
    
catch ME
%    keyboard;
    rethrow(ME)
    % Clean subsystem if error
    Simulink.SubSystem.deleteContents(TCblock_id);
end

% Break the link to library to allow subsystem editing
% set_param(TCblock_id,'LinkStatus','none')

end

function new_str = remove_unsopported_characters(str)
    old = {'\','/',' ','.','?'};
    new = '_';
    new_str = str;
    for i = 1 : length(old)
        new_str = strrep(new_str,old{i},new);
    end    
end


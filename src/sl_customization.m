function sl_customization(cm)
cm.addCustomMenuFcn('Simulink:ContextMenu', @getMyMenuItems);
end

%% Define the custom menu function.
function schemaFcns = getMyMenuItems(~)
schemaFcns = {@getItem1,@getItem2,@getItem3};
end

%% Define the schema function for first menu item.
function schema = getItem1(~)
schema = sl_action_schema;
schema.label = 'Add terminators to outputs';
schema.callback = @AddTerminatorToOutputs;
end

%% Define the schema function for second menu item.
function schema = getItem2(~)
schema = sl_action_schema;
schema.label = 'Add ground to inputs';
schema.callback = @AddGroundToInputs;
end

%% Define the schema function for second menu item.
function schema = getItem3(~)
schema = sl_action_schema;
schema.label = 'Add vector concatenate to outputs';
schema.callback = @AddVecConcatenateToOutputs;
end

%% Define the change color function
function AddTerminatorToOutputs(~)
    % Get selected blocks
    MyBlocks = find_system(gcs,'Selected','on');
    % Use only the blocks that are selected in the current system (discard
    % selected blocks from other subsytems)
    temp = strrep(MyBlocks,sprintf('%s/',gcs),'');
    idx = contains(temp,'/');
    MyBlocks = MyBlocks(~idx);
    
    if isempty(MyBlocks)
        % Assign currently selected block (if none were filtered)
        MyBlocks = {gcb};
    end

    % Loop over the selected blocks
    for i = 1 : length(MyBlocks)
        blk_i       = MyBlocks{i};
        block_h     = get_param(blk_i,'Handle');
    
        parent_path = get_param(block_h,'Parent');

        PortHandles         = get_param(block_h,'PortHandles');
        PortConnectivity    = get_param(block_h,'PortConnectivity');

        n_in = length(PortHandles.Inport);


        for j = 1: length(PortHandles.Outport)
            % Check if port is connected
            if ~isempty(PortConnectivity(n_in + j).DstBlock)
                continue
            end
            % Get port handle
            out_i = PortHandles.Outport(j);

            % Propagate signals
            signal_name = get(out_i,'PropagatedSignals');

            % Get port position 
            out.pos = get_param(out_i,'Position');


            % Create terminator
            Position = [out.pos + [200 -7], out.pos+[230 7]];
            terminator_i  = add_block('simulink/Commonly Used Blocks/Terminator',...
                                      sprintf('%s/Terminator',parent_path),...
                                      'MakeNameUnique','on',...
                                      'Position',       Position);

            in = get_param(terminator_i,'PortHandles');
            add_line(parent_path,out_i,in.Inport);

            if ~isempty(signal_name)
                set_param(terminator_i,'Name',sprintf('Terminator_%s',signal_name));
            end

        end
    end
end

%% Define the change color function
function AddGroundToInputs(~)
    % Get selected blocks
    MyBlocks = find_system(gcs,'Selected','on');
    % Use only the blocks that are selected in the current system (discard
    % selected blocks from other subsytems)
    temp = strrep(MyBlocks,sprintf('%s/',gcs),'');
    idx = contains(temp,'/');
    MyBlocks = MyBlocks(~idx);
    
    if isempty(MyBlocks)
        % Assign currently selected block (if none were filtered)
        MyBlocks = {gcb};
    end

    % Loop over the selected blocks
    for i = 1 : length(MyBlocks)
        blk_i       = MyBlocks{i};
        block_h     = get_param(blk_i,'Handle');
        block_o     = get(block_h);
%         ports       = block_o.Ports;
%         in_ports    = ports(1);
        parent_path = get_param(block_h,'Parent');
        
        
        
        blocks      = block_o.Blocks;
        
        count_in = 1;
        for j = 1 : length(blocks)
            
            block_j = get(get_param([block_o.Path '/' block_o.Name '/' blocks{j}],'handle'));
            if strcmp('Inport',block_j.BlockType)
                
                % Check if port is connected
                if block_o.PortConnectivity(count_in).SrcBlock ~= -1
                    continue
                end
                
                % Get destination block for the input
                dstblk = get(block_j.PortConnectivity.DstBlock);
                if strcmp('BusSelector',dstblk.BlockType)
                    in_names = dstblk.OutputSignalNames;
                    in_names = strrep(in_names,'>','');
                    in_names = strrep(in_names,'<','');
                    n_inputs = length(in_names);
                else
                    continue
                end
                inPort     = get(block_o.PortHandles.Inport(count_in));
                count_in = count_in + 1;
                
                % Create bus creator
                bus_id = sprintf('%s/Bus_%s',parent_path,block_j.Name);
                left = inPort.Position(1);
                top  = inPort.Position(2);
                bus_h  = add_block(...
                          'simulink/Commonly Used Blocks/Bus Creator',bus_id,...
                          'Position',       [left-25 top-10*n_inputs left-20 top+10*n_inputs],...
                          'Inputs',         num2str(n_inputs)...
                          );
                bus_o = get(bus_h);
                %add line from bus to input
                add_line(parent_path,bus_o.PortHandles.Outport,inPort.Handle);
                
                for k = 1 : n_inputs
                    in_pos = bus_o.PortConnectivity(k).Position;
                    in_handle = bus_o.PortHandles.Inport(k);
                    % Create terminator
                    ground_pos = [in_pos - [160 7], in_pos - [145 -7]];
                    ground_k  = add_block('simulink/Commonly Used Blocks/Ground',...
                                              sprintf('%s/Ground',parent_path),...
                                              'MakeNameUnique','on',...
                                              'Position',       ground_pos);

                    ground_out = get_param(ground_k,'PortHandles');
                    % Add name to signal
                    set(ground_out.Outport, 'SignalNameFromLabel', in_names{k});
                    add_line(parent_path,ground_out.Outport,in_handle);
                    
                end
                
            end
        end

    end
end

%%
function AddVecConcatenateToOutputs(~)
    
    % Get selected blocks
    MyBlocks = find_system(gcs,'Selected','on');
    % Use only the blocks that are selected in the current system (discard
    % selected blocks from other subsytems)
    temp = strrep(MyBlocks,sprintf('%s/',gcs),'');
    idx = contains(temp,'/');
    MyBlocks = MyBlocks(~idx);

    if isempty(MyBlocks)
        % Assign currently selected block (if none were filtered)
        MyBlocks = {gcb};
    end
    
    % Loop over the selected blocks
    for i = 1 : length(MyBlocks)
        blk_i       = MyBlocks{i};
        block_h     = get_param(blk_i,'Handle');
        
        parent_path = get_param(block_h,'Parent');

        PortHandles         = get_param(block_h,'PortHandles');
        PortConnectivity    = get_param(block_h,'PortConnectivity');

        n_in = length(PortHandles.Inport);
        

        for j = 1: length(PortHandles.Outport)
            % Check if port is connected
            if ~isempty(PortConnectivity(n_in + j).DstBlock)
                continue
            end
            % Get port handle
            out_i = PortHandles.Outport(j);

            

            % Get port position 
            out.pos = get_param(out_i,'Position');


            % Create vector concatenate
            Position = [out.pos + [200 -10], out.pos+[205 10]];
            vecconcat(j)  = add_block('simulink/Commonly Used Blocks/Vector Concatenate',...
                                      sprintf('%s/Vector Concatenate',parent_path),...
                                      'MakeNameUnique','on',...
                                      'NumInputs','1',...
                                      'Position',       Position);
                                              
            vcph = get_param(vecconcat(j),'PortHandles');
            add_line(parent_path,out_i,vcph.Inport);
            
            %Propagate signals
            signal_name = get_param(vcph.Inport,'Name');
            temp = strrep(signal_name,'<','');
            signal_name = strrep(temp,'>','');
            set_param(vcph.Outport,'Name',signal_name);
            if ~isempty(signal_name)
                set_param(vecconcat(j),'Name',signal_name);
            end
        end
        
    end
end




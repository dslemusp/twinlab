function TCinfo = loadTCinfo(file_path)

    if exist('file_path','var')
        if ~isfile(file_path)
            error('File was not found. Please input complete path!')
        end
    else
        % Get file and path of the TC project
        [file,path] = uigetfile('*.tsproj');
        file_path = [path file];
        if all(~file_path)
            TCinfo = '';
            return
        end
    end
    % Get xml struct
    TCtree = xml2struct(file_path);

    % Check if there are multiple devices in the IO
    IO_devs = TCtree.TcSmProject.Project.Io.Device;
    if numel(IO_devs) > 1
        for idx = 1 : numel(IO_devs)
            IO_dev = IO_devs{idx};
            if isfield(IO_dev.Attributes,'AmsNetId')
                % Use the first device with netID
                break
            end
        end
    else
        IO_dev = IO_devs;
    end
    
    
    % Get master name
    master_name = IO_dev.Name.Text;
    boxes       = IO_dev.Box;

    slaves = get_slaves(master_name,boxes);

    TCinfo.master.name = master_name;
    TCinfo.slaves = slaves;
end

function new_datatype = mapDataType(TC_data_type)
    Data_type_mapping = {'BOOL','boolean';
                         'BYTE','uint8';
                         'WORD','uint16';
                         'DWORD','uint32';
                         'SINT','int8';
                         'USINT','uint8';
                         'INT','int16';
                         'UINT','uint16';
                         'DINT','int32';
                         'UDINT','uint32';
                         'REAL','single';
                         'LREAL','double';
                         'TIME','uint32';
                         'TIME_OF_DAY','uint32';
                         'DATE','uint32';
                         'DATE_AND_TIME','uint32';
                        };
    
    idx = strcmpi(TC_data_type,Data_type_mapping(:,1));
    
    new_datatype = Data_type_mapping{idx,2};

end

function slaves = get_slaves(master_name,boxes)
    slaves = {};
    for i = 1 : length(boxes)
        if length(boxes) == 1
            box = boxes;
        else 
            box = boxes{i};
        end
        
        slave.name = box.Name.Text;

        % Get slave PDOs categories
        pdos = box.EtherCAT.Pdo;
        
        % Check if there are pdos
        if isempty(pdos)
            warning('Slave %s does not have pdo mapping. Skipping!',slave.name);
            continue
        end
        
        % Loop over the PDOs categories
        inputs  = {};
        outputs = {};
        for j = 1 : length(pdos)
            pdo = pdos{j};
            pdo_name = pdo.Attributes.Name;
            if isfield(pdo.Attributes,'SyncMan')
                if isfield(pdo.Attributes,'InOut')
                    % Inputs
                    channels   = {};
                    for k = 1 : numel(pdo.Entry)
                        if numel(pdo.Entry) > 1
                            entry = pdo.Entry{k};
                        else
                            entry = pdo.Entry;
                        end
                        channel.Name      = entry.Attributes.Name;
%                         channel.PDO       = pdo_name;
                        channel.DataType  = mapDataType(entry.Type.Text);
                        channel.FullName  = sprintf('TIID^%s^%s^%s^%s',...
                                            master_name,slave.name,pdo_name,channel.Name);
                        channels = [channels channel];
                    end
                    ins.pdo_name = pdo_name;
                    ins.channels = channels;
                    inputs = [inputs ins];
                else
                    % Outputs
                    channels  = {};
                    for k = 1 : numel(pdo.Entry)
                        if numel(pdo.Entry) > 1
                            entry = pdo.Entry{k};
                        else
                            entry = pdo.Entry;
                        end
                        channel.Name      = entry.Attributes.Name;
%                         channel.PDO       = pdo_name;
                        channel.DataType  = mapDataType(entry.Type.Text);
                        channel.FullName  = sprintf('TIID^%s^%s^%s^%s',...
                                            master_name,slave.name,pdo_name,channel.Name);
                        channels = [channels channel];
                    end
                    outs.pdo_name = pdo_name;
                    outs.channels = channels;
                    outputs = [outputs outs];
                end
            end            
        end
        slave.inputs    = inputs;
        slave.outputs   = outputs;
        
        % Get nested slaves if any (If current IO is a box)
        if isfield(box,'Box')
            box_name = sprintf('%s^%s',master_name,slave.name);
            slaves = get_slaves(box_name,box.Box);
        end
        
        slaves = [slaves slave];
    end
end